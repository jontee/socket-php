<?php
include_once 'TinyRedisClient.php';
include_once 'Emitter.php';

$redis = new TinyRedisClient(); // Using the Redis extension provided client
// $redis->connect('127.0.0.1', '6379');
$emitter = new SocketIO\Emitter($redis);
$emitter->emit('event', 'payload str');