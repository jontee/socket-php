<?php
/**
 * This file is part of the Elephant.io package
 *
 * For the full copyright and license information, please view the LICENSE file
 * that was distributed with this source code.
 *
 * @copyright Wisembly
 * @license   http://www.opensource.org/licenses/MIT-License MIT License
 */

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require __DIR__ . '/vendor/autoload.php';
include_once 'config.php';
// $mobile = $message->get_mobile(2);
// echo "Sender: ".$mobile['mobile'];
// var_dump($mobile);
// exit;

$client = new Client(new Version1X('http://localhost:3000'));

$client->initialize();

$data_arr = $message->sort_message();
// print_r($data_arr);
// exit;
foreach ($data_arr as $data)
{
	$client->emit('broadcast', $data);
}
$client->close();
