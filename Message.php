<?php
class Message
{
    private $db;
 
    function __construct($DB_con)
    {
      $this->db = $DB_con;
    }
 
    public function register($fname,$lname,$uname,$umail,$upass)
    {
       try
       {
           $new_password = password_hash($upass, PASSWORD_DEFAULT);
   
           $stmt = $this->db->prepare("INSERT INTO users(user_name,user_email,user_pass) 
                                                       VALUES(:uname, :umail, :upass)");
              
           $stmt->bindparam(":uname", $uname);
           $stmt->bindparam(":umail", $umail);
           $stmt->bindparam(":upass", $new_password);            
           $stmt->execute(); 
   
           return $stmt; 
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }    
    }
 
    public function login($uname,$umail,$upass)
    {
       try
       {
          $stmt = $this->db->prepare("SELECT * FROM users WHERE user_name=:uname OR user_email=:umail LIMIT 1");
          $stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
          $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
          if($stmt->rowCount() > 0)
          {
             if(password_verify($upass, $userRow['user_pass']))
             {
                $_SESSION['user_session'] = $userRow['user_id'];
                return true;
             }
             else
             {
                return false;
             }
          }
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
   }
 
   public function is_loggedin()
   {
      if(isset($_SESSION['user_session']))
      {
         return true;
      }
   }
 
   public function redirect($url)
   {
       header("Location: $url");
   }
 
   public function logout()
   {
        session_destroy();
        unset($_SESSION['user_session']);
        return true;
   }


    public function sort_message()
    {
       try
       {
          $result_arr = [];
          $stmt = $this->db->prepare("SELECT sender_id, recipient_id, messages, created FROM messages WHERE status < 1 AND deleted = 0 ");
          $stmt->execute();
          // $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
          $msgRow=$stmt->fetchAll();
          if($stmt->rowCount() > 0)
          {
            foreach ($msgRow as $data)
            {
              // if ($data['online'] != 0)
              // {
              $sender = $this->get_mobile($data['sender_id']);
              $recipient = $this->get_mobile($data['recipient_id']);
              $msg = [
                'sender' => $sender['mobile'],
                'recipient' => $recipient['mobile'],
                'message' => $data['messages'],
                'created' => $data['created']
              ];
              array_push($result_arr, $msg);
              // }

            }
          }
          return $result_arr;
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
   }

   public function get_mobile($id)
   {
    $stmt = $this->db->prepare("SELECT mobile FROM users WHERE id=:id");

    // $stmt = $this->db->prepare("SELECT mobile, online FROM users WHERE id=:id AND online != 0");

    $stmt->execute(array(":id"=>$id));
    $editRow=$stmt->fetch(PDO::FETCH_ASSOC);
    return $editRow;
   }


}
?>